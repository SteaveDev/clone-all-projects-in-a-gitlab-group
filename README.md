# Clone all projects in a gitlab group

This repository contains custom commands that a newcomer needs to install on his laptop.

## Getting started

- `init.sh`: includes every script in this repository. This file is included in you bash/zsh RC file.
- `setup.sh`: this file should be run one-shot directly after pulling this repository to add it to the RC file.
- Insert your gitlab private token in argument and group ID also
    - `python3 clone.py <PRIVATE TOKEN> <GROUP ID>`

## Installation

- Install pip :
    - https://pip.pypa.io/en/stable/installation/
- Run : `pip install`

