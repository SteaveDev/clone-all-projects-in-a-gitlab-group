import sys
import os
import requests
import json
import git

if(len(sys.argv) < 2):
    print('**********************************************')
    print('Gitlab private token is missing in argument...')
    print('**********************************************')
    exit(-1)

def createFolderAtPath(path):
    try:
        os.makedirs(path, exist_ok=True)
        print(f'Folder created ---> {path}')
    except FileExistsError:
        print('File already exists...')

PRIVATE_TOKEN = str(sys.argv[1])
GITLAB_GROUP_ID = str(sys.argv[2])
header={'PRIVATE-TOKEN':PRIVATE_TOKEN}
page = 1
path = './MY_FOLDER/'
createFolderAtPath(path)

while True:
    url = f'http://www.gitlab.com/api/v4/groups/{GITLAB_GROUP_ID}/descendant_groups?per_page=5&page={page}'
    response = requests.get(url,headers=header)

    loadedGroup = json.loads(response.content)

    for group in loadedGroup:
        print(f"id={group['id']}, name={group['name']}, path={group['full_path']}\n\n")

        project_url = f"http://www.gitlab.com/api/v4/groups/{group['id']}/projects?per_page=100"
        createFolderAtPath(path + group['full_path'])
        responseProjects = requests.get(project_url,headers=header)

        loadedProject = json.loads(responseProjects.content)

        for project in loadedProject:
            git.Git(path + project['namespace']['full_path']).clone(project['ssh_url_to_repo'])
            print(f"{project['ssh_url_to_repo']} ...")
        print('\n\n******************************************************************')

    print(f'\n\nPAGE ==> {page}')
    if(response.headers['X-Total-Pages'] == response.headers['X-Page']):
        break
    page = page + 1

